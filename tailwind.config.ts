module.exports = {
  plugins: [require('daisyui')],
  daisyui: {
    themes: ['system', 'light', 'dark']
  }
}
