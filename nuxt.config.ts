// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: ['@pinia/nuxt', '@nuxtjs/tailwindcss', '@nuxtjs/color-mode', '@vueuse/nuxt', 'nuxt-icon'],
  pinia: {
    autoImports: ['defineStore']
  },
  imports: {
    dirs: ['composables', 'stores'],
    imports: [
      {
        name: 'Form',
        from: 'vee-validate'
      },
      {
        name: 'Field',
        from: 'vee-validate'
      },
      {
        name: 'useField',
        from: 'vee-validate'
      },
      {
        name: 'useForm',
        from: 'vee-validate'
      },
      {
        name: 'validate',
        from: 'vee-validate'
      }
    ]
  },
  colorMode: {
    preference: 'system', // default theme
    dataValue: 'theme', // activate data-theme in <html> tag
    classSuffix: ''
  },
  runtimeConfig: {
    public: {
      app: {
        name: 'Nuxt3 Starter',
        description: 'Just another Nuxt3 project starter',
        publicIconPath: '/assets/icon.png',
        twitterHandle: '@hrdtr_'
      }
    }
  }
})
